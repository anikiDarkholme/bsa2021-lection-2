﻿using System;

namespace RabbitMQ.Wrapper
{
    public interface IMessageBroker : IDisposable
    {
        void ListenToQueue(string queueName);
        void SendMessageToQueue(string queueName, string message);
    }
}