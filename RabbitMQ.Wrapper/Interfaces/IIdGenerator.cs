﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IIdGenerator
    {
        char[] GenerateID(int charCount);
    }
}
