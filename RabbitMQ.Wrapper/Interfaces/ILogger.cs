﻿using System;

namespace RabbitMQ.Wrapper
{
    public interface ILogger 
    {
        void Log(string message);
    }
}