﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using System;
using System.Linq;
using System.Text;
using System.Threading;

namespace RabbitMQ.Wrapper
{
    public class MessageBroker : IMessageBroker
    {
        protected readonly IConnection _connection;

        protected readonly IModel _channel;

        protected readonly ILogger _logger;

        protected string replyQueueName;

        public MessageBroker(IConnectionFactory connectionFactory, ILogger logger)
        { 
            _logger = logger;

            _connection = connectionFactory.CreateConnection();

            _channel = _connection.CreateModel();
        }

        public void ListenToQueue(string queueName)
        {
           replyQueueName = 
            _channel.QueueDeclare(queue: queueName, durable: false, exclusive: false, autoDelete: true, arguments: null).QueueName;

            _channel.BasicQos(0, 1, false);

            var consumer = new EventingBasicConsumer(_channel);

            _channel.BasicConsume(queue: queueName, autoAck: false, consumer: consumer);

            _logger.Log($" [x] Connected queue {queueName}");
        }

        public void SendMessageToQueue(string queueName, string message)
        {
            _channel.QueueDeclare(queue: queueName, durable: false, exclusive: false, autoDelete: true, arguments: null);

            var messageBytes = Encoding.UTF8.GetBytes(message);

            _channel.BasicPublish(
                exchange: "",
                routingKey: queueName,
                basicProperties: null,
                body: messageBytes);

            _logger.Log($" [x] Sent {message} to {queueName} \n");
        }



        public virtual void Dispose()
        {
            _channel.QueuePurge(replyQueueName);
            _channel.Close();
            _channel.Dispose();

            _connection.Close();
            _connection.Dispose();
        }
    }
}
