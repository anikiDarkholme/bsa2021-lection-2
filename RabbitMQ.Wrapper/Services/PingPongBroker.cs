﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace RabbitMQ.Wrapper
{
    public class PingPongBroker : MessageBroker
    {
        protected readonly string _playerId;
        private readonly IIdGenerator idGenerator;
        private PlayerState _state;
        private readonly string gameQueueName;
        public string State => _state.ToString().ToLower(); 
       
        public PingPongBroker(IConnectionFactory connectionFactory,
                             ILogger logger,
                             IIdGenerator idGenerator,
                             PlayerState state)
            : base(connectionFactory, logger)
        {


            this._playerId = new string(idGenerator.GenerateID(1));
            this.idGenerator = idGenerator;
            this._state = state;

            this.gameQueueName = State + "_queue";
        }

        public void ListenToQueue()
        {
            _channel.QueueDeclare(queue: gameQueueName, durable: false, exclusive: false, autoDelete: true, arguments: null);

            _channel.BasicQos(0, 1, false);

            var consumer = new EventingBasicConsumer(_channel);

            _channel.BasicConsume(queue: gameQueueName, autoAck: false, consumer: consumer);

            consumer.Received += Receive;
        }

        private void Receive(object model, BasicDeliverEventArgs ea)
        {
            string message = GetEAMessage(ea);

            _logger.Log($" [{_playerId}] {message}");

            Thread.Sleep(2500);

            SendMessageToGameQueue(ea.BasicProperties.ReplyTo, $"{State[..1].ToUpper()}{State[1..]}!");

            _channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
        }

        private string GetEAMessage(BasicDeliverEventArgs ea)
        {
            var body = ea.Body.ToArray();

            string message = Encoding.UTF8.GetString(body);

            string correlationID = string.Empty;

            if (ea.BasicProperties.IsCorrelationIdPresent())
                correlationID = "from" + ea.BasicProperties.CorrelationId;

            return $"{message} | {correlationID} {DateTime.Now}";
        }

        public void SendMessageToGameQueue(string queueName, string message)
        {
            _channel.QueueDeclare(queue: queueName, durable: false, exclusive: false, autoDelete: true, arguments: null);

            var messageProps = _channel.CreateBasicProperties();

            messageProps.ReplyTo = this.gameQueueName;

            messageProps.CorrelationId = $"{State}_{_playerId}";

            var messageBytes = Encoding.UTF8.GetBytes(message);

            _channel.BasicPublish(
                exchange: "",
                routingKey: queueName,
                basicProperties: messageProps,
                body: messageBytes);
        }

        public override void Dispose()
        { 
            _channel.Close();
            _channel.Dispose();

            _connection.Close();
            _connection.Dispose();
        }
    }
}
