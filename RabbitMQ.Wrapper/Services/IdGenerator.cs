﻿using RabbitMQ.Wrapper.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RabbitMQ.Wrapper
{
    public class IdGenerator : IIdGenerator
    {
        public char[] GenerateID(int charCount)
        {
            if (charCount < 1 || charCount > 5)
                return new char[] { };

           return Guid.NewGuid().ToString().Take(charCount).ToArray();
        }
    }
}
