﻿using System;
using RabbitMQ.Wrapper;

namespace Pinger
{
    class Pinger
    {
        private readonly IMessageBroker _messageBroker;

        public Pinger(IMessageBroker messageBroker)
        {
            this._messageBroker = messageBroker;
        }

        internal void Start()
        {

            (_messageBroker as PingPongBroker).ListenToQueue();
            
            (_messageBroker as PingPongBroker).SendMessageToGameQueue("pong_queue", $"Ping!");
            
            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }  
    
    }
}
