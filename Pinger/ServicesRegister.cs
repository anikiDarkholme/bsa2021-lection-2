﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Linq;
using RabbitMQ.Wrapper;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Client;

namespace Pinger
{
    class ServicesRegister
    {
        public static IContainer RegisterContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterType<IdGenerator>()
                .As<IIdGenerator>();

            builder.RegisterType<ConsoleLogger>()
              .As<ILogger>();

            builder.RegisterType<ConnectionFactory>()
              .As<IConnectionFactory>();

            builder.RegisterType<PingPongBroker>()
             .As<IMessageBroker>()
             .WithParameter(new TypedParameter(typeof(PlayerState), PlayerState.Ping));

            builder.RegisterType<Pinger>()
                .AsSelf();

            return builder.Build();
        }

    }
}
