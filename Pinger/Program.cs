﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Text;
using System.Threading;
using System.Linq;
using RabbitMQ.Wrapper;
using Autofac;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = ServicesRegister.RegisterContainer();

            using (var scope = container.BeginLifetimeScope())
            {
                var ping = scope.Resolve<Pinger>();
                (ping as Pinger).Start();
            }
        }
    }
}
