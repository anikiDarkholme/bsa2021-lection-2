﻿using System;
using RabbitMQ.Wrapper;

namespace Ponger
{
    class Ponger
    {
        private readonly IMessageBroker _messageBroker;

        public Ponger(IMessageBroker messageBroker)
        {
            this._messageBroker = messageBroker;
        }

        internal void Start()
        { 
           (_messageBroker as PingPongBroker).ListenToQueue();

           Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }
    }
}
