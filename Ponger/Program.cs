﻿using System.Linq;
using System.Text;
using System.Threading;
using Autofac;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = ServicesRegister.RegisterContainer();

            using (var scope = container.BeginLifetimeScope())
            {
                var pong = scope.Resolve<Ponger>();
                (pong as Ponger).Start();
            }
        }
    }
}
